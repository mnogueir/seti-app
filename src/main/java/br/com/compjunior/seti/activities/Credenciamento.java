package br.com.compjunior.seti.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.compjunior.seti.R;
import br.com.compjunior.seti.models.EventoModel;
import br.com.compjunior.seti.models.EventoModelFactory;
import br.com.compjunior.seti.models.ParticipanteModel;
import br.com.compjunior.seti.persistence.FileManager;

public class Credenciamento extends AppCompatActivity {

    /**
     * Lista de todos os eventos cadastrados no sistema.
     */
    private List<EventoModel> eventos;

    /**
     * Evento que será credenciado agora.
     */
    private static EventoModel eventoAtivo;

    /**
     * Objeto usado para armazenar os objetos num arquivo serializado.
     */
    private FileManager manager;

    /**
     * Lista de cursos que foram credenciados.
     */
    private static ArrayList<EventoModel> credenciados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credenciamento);
        eventos = new ArrayList<EventoModel>();
        try {// Carrega o arquivo que armazena os dados serialidados
            manager = new FileManager("presencas.dat", this);
            credenciados = (ArrayList<EventoModel>) manager.load();
        } catch (Exception e) {
            credenciados = new ArrayList<EventoModel>();
        }
        // Adiciona evento de onchange
        Spinner spinnerData = (Spinner) findViewById(R.id.spinner);
        // Quando uma data for selecionada
        spinnerData.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Pega a data selecionada
                Spinner spinnerData = (Spinner) findViewById(R.id.spinner);
                String item = spinnerData.getSelectedItem().toString().split(" - ")[1];
                // Carrega os eventos que ocorrerão nessa data
                Credenciamento.this.loadEventos(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Não faz nada
            }
        });
        // Carrega todos os eventos
        carregarEventos();
    }

    /**
     * Abre o arquivo de eventos e carrega todos os dados que estiverem lá.
     */
    private void carregarEventos() {
        try {
            // Criador de instâncias de eventos
            EventoModelFactory factory = new EventoModelFactory();
            // Carrega todos os eventos
            FileManager manager = new FileManager("cursos-inscricoes.data", this);
            String jsonEventos = manager.read();
            // Cria um array de objetos json
            JSONArray array = new JSONArray(jsonEventos);
            // Extraí os objetos json
            for (int i = 0; i < array.length(); ++i) {
                JSONObject obj = array.getJSONObject(i);
                EventoModel evento = factory.construct(obj);
                eventos.add(evento);
            }
        } catch (Exception e) {
            Log.d("Exception: ", e.getMessage());
        }
    }

    /**
     * Carrega todos os eventos que acontecerão no dia especificado e os coloca
     * no Spinner de eventos.
     *
     * @param data data dos eventos
     */
    public void loadEventos(String data)
    {
        ArrayList<String> listaEventos = new ArrayList<String>();
        Spinner spinner = (Spinner) findViewById(R.id.spinner2);
        try {
            // Transforma a string da data em um objeto Date
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date date = format.parse(data);
            // Procura todos os eventos que acontecerão nessa data.
            for (int i = 0; i < eventos.size(); ++i)  {
                if (date.equals(eventos.get(i).getData())) {
                    // Adiciona evento ao spinner
                    listaEventos.add(eventos.get(i).getNome());
                }
            }
            String[] arrEventos = new String[listaEventos.size()];
            arrEventos = listaEventos.toArray(arrEventos);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_dropdown_item, arrEventos);
            spinner.setAdapter(adapter);
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
    }

    public void iniciarQr(View view) {
        try {
            // Pega a data selecionada
            Spinner spinnerEvento = (Spinner) findViewById(R.id.spinner2);
            String item = spinnerEvento.getSelectedItem().toString();
            // Procura o item selecionado
            for (int i = 0; i < eventos.size(); ++i) {
                if (eventos.get(i).getNome().equals(item)) {
                    eventoAtivo = eventos.get(i);
                    // Checa se o evento ativo está armazenado no arquivo serializado
                    if (!credenciados.contains(eventoAtivo))
                        credenciados.add(eventoAtivo);
                    break;
                }
            }
            // Inicia a leitura do QR code
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
        } catch (Exception e) {
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult.getContents() != null) {
            // Checa se o usuário está inscrito no evento
            try {
                Long cpf = Long.parseLong(scanResult.getContents());
                int pos = -1;
                if (eventoAtivo == null)
                    Toast.makeText(this, "Você não escolheu nenhum evento para credenciar!", Toast.LENGTH_LONG).show();
                else {
                    // Procura o evento ativo.
                    for (int i = 0; i < eventoAtivo.getParticipantes().size(); ++i) {
                        if (eventoAtivo.getParticipantes().get(i).getCpf().equals(cpf)) {
                            pos = i;
                            break;
                        }
                    }
                    Log.d("pos", Integer.toString(pos));
                    if (pos < 0) {
                        // Mostra mensagem de "PEGA O PENETRA!!"
                        Toast.makeText(this, "Esse participante não está inscrito nesse evento!", Toast.LENGTH_LONG).show();
                    }else {
                        final ParticipanteModel participante = eventoAtivo.getParticipantes().get(pos);
                        // Confirma a presença do participante.
                        new AlertDialog.Builder(this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Confirmação do participante")
                                .setMessage("Nome: " + participante.getNome() + "\nEvento: " + eventoAtivo.getNome())
                                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        participante.marcarPresenca();
                                        try {
                                            manager.save(credenciados);
                                            Toast.makeText(Credenciamento.this, "Presença confirmada com sucesso!", Toast.LENGTH_LONG).show();
                                        } catch (Exception e) {
                                            Toast.makeText(Credenciamento.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                })
                                .setNegativeButton("Não", null)
                                .show();
                    }
                }
            } catch (Exception e) {
                Log.d("Erro", e.getMessage());
                Toast.makeText(this, "QRCode inválido!", Toast.LENGTH_LONG).show();
            }
        }
    }

}
