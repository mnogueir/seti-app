package br.com.compjunior.seti.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.compjunior.seti.R;
import br.com.compjunior.seti.models.EventoModel;
import br.com.compjunior.seti.network.DataRetriever;
import br.com.compjunior.seti.network.Receiver;
import br.com.compjunior.seti.persistence.FileManager;

public class TelaInicial extends AppCompatActivity {

    /**
     * URL usada para sincronizar os dados de cursos e inscrições.
     */
    //private final String URL_SINCRONIZAR = "http://compjunior.com.br/seti/api/getdata/";
    private final String URL_SINCRONIZAR = "http://compjunior.com.br/seti/api/getinfo";

    /**
     * URL usada para enviar os dados para o servidor.
     */
    private final String URL_ENVIAR = "http://compjunior.com.br/seti/api/setdata";

    /**
     * Objeto usado para acessar dados do servidor.
     */
    private DataRetriever dataRetriever;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_inicial);
        dataRetriever = new DataRetriever(this);
    }

    public void sincronizarServer(View v) {
        Context context = this;
        dataRetriever.getData(URL_SINCRONIZAR, new Receiver() {
            @Override
            public void onDataReceived(String data) {
                try {
                    JSONArray json = new JSONArray(data);
                    // Salva o json num arquivo
                    FileManager f = new FileManager("cursos-inscricoes.data", TelaInicial.this);
                    f.write(json.toString());
                    Toast.makeText(TelaInicial.this, "Dados sincronizados com sucesso!", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.d("Error: ", e.getMessage());
                }
            }
        });
    }

    public void iniciarCredenciamento(View v) {
        try {
            Intent intent = new Intent(this, Credenciamento.class);
            startActivity(intent);
        } catch (Exception e) {
            Log.d("ERRO: ", e.getMessage());
        }
    }

    public void enviarDados(View v) {
        try {
            // Carrega arquivo de dados
            FileManager manager = new FileManager("presencas.dat", this);
            ArrayList<EventoModel> modelos = (ArrayList<EventoModel>) manager.load();
            String json = new Gson().toJson(modelos);
            JSONArray jsonData = new JSONArray(json);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, URL_ENVIAR,
                    jsonData, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Toast.makeText(TelaInicial.this, "Dados enviados com sucesso!", Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(request);
        } catch (Exception e) {
            longInfo(e.getMessage());
            Toast.makeText(this, "Erro ao enviar os dados para o servidor!", Toast.LENGTH_LONG).show();
        }
    }

    public static void longInfo(String str) {
        if(str.length() > 4000) {
            Log.i("TAG", str.substring(0, 4000));
            longInfo(str.substring(4000));
        } else
            Log.i("TAG", str);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ImageView imageBottom = (ImageView) findViewById(R.id.imageView2);
        int height = imageBottom.getHeight();
        // Pega o tamanho da tela
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenHeight = size.y;
        // Coloca a imagem de fundo no lugar certo
        imageBottom.setTop(screenHeight - height);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tela_inicial, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
