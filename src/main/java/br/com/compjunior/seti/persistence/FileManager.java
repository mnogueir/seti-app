package br.com.compjunior.seti.persistence;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Classe responsável por salvar arquivos no dispositivo de armazenamento do android.
 *
 * Created by matheus on 9/15/15.
 */
public class FileManager {

    /**
     * Arquivo que será usado.
     */
    protected String file;

    /**
     * Contexto do aplicativo.
     */
    protected Context context;

    /**
     * Cria um gerenciador para o arquivo.
     * @param file arquivo que será usado para armazenar os dados.
     * @param context contexto do aplicativo.
     */
    public FileManager(String file, Context context) {
        this.file = file;
        this.context = context;
    }

    /**
     * Escreve uma string no arquivo.
     * @param input dados que serão escritos no arquivo.
     */
    public void write(String input) {
        // Cria stream para escrever no arquivo
        FileOutputStream out;
        try {
            // Abre a stream
            out = context.openFileOutput(this.file, context.MODE_PRIVATE);
            out.write(input.getBytes());
            out.close();
        } catch (Exception e) {
            Log.d("Erro IO: ", e.getMessage());
        }
    }

    /**
     * Lê o conteúdo de um arquivo.
     */
    public String read() {
        StringBuilder sb = new StringBuilder();
        int n;
        try {
            byte[] buffer = new byte[1024];
            FileInputStream in = context.openFileInput(this.file);
            while ((n = in.read(buffer)) != -1) {
                sb.append(new String(buffer, 0, n));
            }
            return sb.toString();
        } catch (Exception e) {
            Log.d("Erro IO: ", e.getMessage());
        }
        return null;
    }

    /**
     * Salva um objeto serializavel.
     * @param obj objeto que será salvo.
     */
    public void save(Serializable obj) throws Exception {
//        File f = new File(Environment.getExternalStorageDirectory(), "/" + this.file);
//        FileOutputStream fout = new FileOutputStream(f);
//        ObjectOutputStream out = new ObjectOutputStream(fout);
//        out.writeObject(obj);
        FileOutputStream fos = context.openFileOutput(this.file, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(obj);
        os.close();
        fos.close();
    }

    /**
     * Carrega um arquivo de objetos serializados.
     * @return objeto serializado.
     */
    public Serializable load() throws Exception {
//        File f = new File(Environment.getExternalStorageDirectory(), "/" + this.file);
//        FileInputStream fin = new FileInputStream(f);
//        ObjectInputStream out = new ObjectInputStream(fin);
//        return (Serializable) out.readObject();
        FileInputStream fis = context.openFileInput(this.file);
        ObjectInputStream is = new ObjectInputStream(fis);
        Serializable obj = (Serializable) is.readObject();
        is.close();
        fis.close();
        return obj;
    }

}
