package br.com.compjunior.seti.network;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Classe responsável por receber dados de uma url.
 *
 * Created by matheus on 9/15/15.
 */
public class DataRetriever {

    /**
     * Instancia do Volley usada para acessar os dados via URL.
     */
    protected RequestQueue requestQueue;

    /**
     * Contexto da aplicação.
     */
    protected Context context;

    /**
     * Cria um novo recebedor de dados.
     * @param context contexto da tela que está chamando esse recebedor.
     */
    public DataRetriever(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
    }

    /**
     * Pega o conteúdo de uma página da internet.
     * @param url url da página.
     */
    public void getData(String url, final Receiver callback) {
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        callback.onDataReceived(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Erro ao tentar obter dados do servidor.", Toast.LENGTH_LONG).show();
                    }
                });
        requestQueue.add(request);
    }

}
