package br.com.compjunior.seti.network;

/**
 * Created by matheus on 9/15/15.
 */
public abstract class Receiver {

    public abstract void onDataReceived(String data);

}
