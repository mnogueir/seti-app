package br.com.compjunior.seti.models;

import org.json.JSONObject;

/**
 * Fabrica de modelos. Constroi modelos a partir de objetos JSON.
 *
 * Created by matheus on 9/15/15.
 */
public class EventoModelFactory {

    /**
     * Constroi uma nova fábrica.
     */
    public EventoModelFactory() {

    }

    /**
     * Constroi um modelo de um evento a partir de um objeto json.
     * @param json objeto em formato json.
     * @return modelo construido.
     */
    public EventoModel construct(JSONObject json) throws Exception {
        EventoModel evento = new EventoModel();
        evento.buildFromJson(json);
        return evento;
    }
}
