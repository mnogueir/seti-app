package br.com.compjunior.seti.models;

import android.content.ContentValues;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Modelo de um evento.
 *
 * Created by matheus on 9/15/15.
 */
public class EventoModel implements Serializable {

    /**
     * Nome do evento.
     */
    private String nome;

    /**
     * Dia do evento.
     */
    private Date data;

    /**
     * Código do evento.
     */
    private int codigo;

    /**
     * Pessoas inscritas no evento.
     */
    private List<ParticipanteModel> participantes;

    /**
     * Constroi um modelo de evento.
     */
    public EventoModel() {
        participantes = new ArrayList<ParticipanteModel>();
    }

    /**
     * Constroi um evento a partir de sua representação JSON.
     * @param json objeto json.
     * @return modelo do evento.
     */
    public void buildFromJson(JSONObject json) throws Exception {
        int id = json.getInt("id");
        String nome = json.getString("nome");
        String dataStr = json.getString("data");
        JSONArray arrParticipantes = json.getJSONArray("inscritos");
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date data = dateFormat.parse(dataStr);

        // Pega todos os participantes do evento
        int length = arrParticipantes.length();
        for (int i = 0; i < length; ++i) {
            JSONObject participante = arrParticipantes.getJSONObject(i);
            long cpf = participante.getLong("cpf");
            String nomeParticipante = participante.getString("nome");
            participantes.add(new ParticipanteModel(cpf, nomeParticipante));
        }

        this.data = data;
        this.nome = nome;
        this.codigo = id;
    }

    public String getNome() {
        return nome;
    }

    public Date getData() {
        return data;
    }

    public int getCodigo() {
        return codigo;
    }

    public List<ParticipanteModel> getParticipantes() {
        return participantes;
    }

    @Override
    public String toString() {
        return "EventoModel [data=" + this.data + ", nome=" + this.nome + ", codigo=" + this.codigo + "]";
    }
}
