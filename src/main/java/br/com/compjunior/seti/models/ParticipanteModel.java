package br.com.compjunior.seti.models;

import android.content.ContentValues;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;

/**
 * Modelo de um participante de um evento.
 *
 * Created by matheus on 9/16/15.
 */
public class ParticipanteModel implements Serializable {

    public static final long serialVersionUID =2901014648623949036L;

    /**
     * CPF do participante.
     */
    private long cpf;

    /**
     * Nome do participante.
     */
    private String nome;

    /**
     * Flag de presença.
     */
    private boolean presente;

    /**
     * Cria um novo participante.
     */
    public ParticipanteModel(long cpf, String nome) {
        this.cpf = cpf;
        this.nome = nome;
        this.presente = false;
    }

    /**
     * Marcar presença no evento.
     */
    public void marcarPresenca() {
        this.presente = true;
    }

    /**
     * Retorna o cpf do participante.
     * @return cpf do participante.
     */
    public Long getCpf() {
        return cpf;
    }

    /**
     * Retorna o nome do participante.
     * @return nome do participante.
     */
    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return "ParticipanteModel [cpf=" + this.cpf + ", nome=" + this.nome + ", presenca=" + this.presente + "]";
    }
}
